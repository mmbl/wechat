/*
   Copyright 2020 XiaochengTech

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package wxpay

import (
	"testing"
)

// 测试支付结果通知
func TestPayNotifyParseParams(t *testing.T) {
	content := "<xml><appid><![CDATA[123]]></appid>\n<bank_type><![CDATA[OTHERS]]></bank_type>\n<cash_fee><![CDATA[594]]></cash_fee>\n<coupon_count><![CDATA[1]]></coupon_count>\n<coupon_fee>6</coupon_fee>\n<coupon_fee_0><![CDATA[6]]></coupon_fee_0>\n<coupon_id_0><![CDATA[2000000100159004567]]></coupon_id_0>\n<fee_type><![CDATA[CNY]]></fee_type>\n<is_subscribe><![CDATA[N]]></is_subscribe>\n<mch_id><![CDATA[1543844571]]></mch_id>\n<nonce_str><![CDATA[cQyQKGn52RcALLxG8WIplGy4J4dk7DKO]]></nonce_str>\n<openid><![CDATA[oC3825smyDMpPLzCzNP5vDbtymZE]]></openid>\n<out_trade_no><![CDATA[200208174606805071672594]]></out_trade_no>\n<result_code><![CDATA[SUCCESS]]></result_code>\n<return_code><![CDATA[SUCCESS]]></return_code>\n<sign><![CDATA[26F32827D84A2945C59FA99D0670526F]]></sign>\n<sub_appid><![CDATA[wxa0add88237a14605]]></sub_appid>\n<sub_is_subscribe><![CDATA[N]]></sub_is_subscribe>\n<sub_mch_id><![CDATA[1604539785]]></sub_mch_id>\n<sub_openid><![CDATA[oC3825smyDMpPLzCzNPhvhbtymZE]]></sub_openid>\n<time_end><![CDATA[20200803474612]]></time_end>\n<total_fee>600</total_fee>\n<trade_type><![CDATA[JSAPI]]></trade_type>\n<transaction_id><![CDATA[4200000572202008042349256325]]></transaction_id>\n</xml>"
	var result NotifyPayBody
	if err := testClient.payNotifyParseParams([]byte(content), &result); err != nil {
		t.Error(err)
	} else {
		t.Log(result)
	}
}
