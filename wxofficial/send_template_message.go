/*
   Copyright 2020 XiaochengTech

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package wxofficial

import (
	"encoding/json"
	"fmt"
	"gitee.com/xiaochengtech/wechat/util"
)

// 模板消息发送接口
// https://mp.weixin.qq.com/advanced/tmplmsg?action=faq&lang=zh_CN
func SendTemplateMessage(
	accessToken string,
	toUserOpenID string,
	templateID string,
	detailURL string,
	topColor string,
	data map[string]SendTemplateMessageItem,
) (rsp SendTemplateMessageResponse, err error) {
	// 构造请求
	url := fmt.Sprintf("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=%s", accessToken)
	body := map[string]interface{}{
		"touser":      toUserOpenID,
		"template_id": templateID,
		"url":         detailURL,
		"topcolor":    topColor,
		"data":        data,
	}
	// 发送数据
	rspData, err := util.HttpPostJson(url, body)
	if err != nil {
		return
	}
	if err = json.Unmarshal(rspData, &rsp); err != nil {
		return
	}
	return
}

type SendTemplateMessageItem struct {
	Value string `json:"value"` // 值
	Color string `json:"color"` // 颜色
}

type SendTemplateMessageResponse struct {
	ErrCode    int64  `json:"errcode"`    // 错误编码
	ErrMessage string `json:"errmsg"`     // 错误信息
	MessageID  int64  `json:"message_id"` // 消息ID
}
